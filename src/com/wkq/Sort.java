package com.wkq;

public interface Sort {
    <E extends Comparable<E>> void sort(E[] arr);
}
