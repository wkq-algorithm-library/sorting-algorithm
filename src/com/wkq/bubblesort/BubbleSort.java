package com.wkq.bubblesort;

import com.wkq.Sort;
import com.wkq.util.ArrayGenerator;
import com.wkq.util.SortingHelper;

/**
 * O(n^2)的排序算法
 * 基本思想：每次比较相邻的两个元素
 * 第i轮开始，arr[n-i,n)已经排好序
 * 第i轮：通过冒泡在arr[n-i-1]位置上放上合适的元素
 * 第i轮结束：arr[n-i-1,n)已排好序
 */
public enum BubbleSort implements Sort {
    INSTANCE;

    @Override
    public <E extends Comparable<E>> void sort(E[] arr) {
        //arr(arr.length-1-i,arr.length)有序,i表示已经排好序的元素个数
        for (int i = 0; i < arr.length - 1; ) {
            int lastSwappedIndex = arr.length - 1 - i;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    SortingHelper.swap(arr, j, j + 1);
                    lastSwappedIndex = j + 1;
                }
            }
            i = arr.length - lastSwappedIndex;
        }
    }

    public static void main(String[] args) {
        int[] dataSize = {10000, 100000};
        for (int n : dataSize) {
            Integer[] arr = ArrayGenerator.generatorRandomArray(n, n);
            SortingHelper.sortTest(BubbleSort.INSTANCE, arr);
        }
    }
}
