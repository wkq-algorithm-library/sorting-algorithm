package com.wkq.bubblesort;

import com.wkq.Sort;
import com.wkq.util.ArrayGenerator;
import com.wkq.util.SortingHelper;

/**
 * 换个方式实现冒泡排序
 * <p>
 * O(n^2)的排序算法
 * 基本思想：每次比较相邻的两个元素
 * 第i轮开始，arr[0,i)已经排好序
 * 第i轮：通过冒泡在arr[i]位置上放上合适的元素
 * 第i轮结束：arr[0，i+1)已排好序
 */
public enum BubbleSort2 implements Sort {
    INSTANCE;

    @Override
    public <E extends Comparable<E>> void sort(E[] arr) {
        //arr[0,i)已排序，每次
        for (int i = 0; i < arr.length - 1; ) {
            int lastSwappedIndex = i;
            for (int j = arr.length - 1; j > i; j--) {
                if (arr[j].compareTo(arr[j - 1]) < 0) {
                    SortingHelper.swap(arr, j, j - 1);
                    lastSwappedIndex = j - 1;
                }
            }
            i = lastSwappedIndex + 1;//i表示已排序元素的数量，因为索引比数量小1，所以+1
        }
    }

    public static void main(String[] args) {
        int[] dataSize = {10000, 100000};
        for (int n : dataSize) {
            Integer[] arr = ArrayGenerator.generatorRandomArray(n, n);
            SortingHelper.sortTest(BubbleSort2.INSTANCE, arr);
        }
    }
}
