package com.wkq.bubblesort;

import com.wkq.Sort;
import com.wkq.selectionsort.SelectionSort;
import com.wkq.util.ArrayGenerator;
import com.wkq.util.SortingHelper;

public enum BubbleSortV1 implements Sort {
    INSTANCE;

    @Override
    public <E extends Comparable<E>> void sort(E[] arr) {
        //arr(arr.length-1-i,arr.length)有序
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j].compareTo(arr[j+1])>0)
                    SortingHelper.swap(arr, j, j + 1);
            }
        }
    }
    public static void main(String[] args) {
        int[] dataSize = {10000, 100000};
        for (int n : dataSize) {
            Integer[] arr = ArrayGenerator.generatorRandomArray(n, n);
            SortingHelper.sortTest(BubbleSortV1.INSTANCE, arr);
        }
    }
}
