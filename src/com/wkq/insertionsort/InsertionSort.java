package com.wkq.insertionsort;

import com.wkq.Sort;
import com.wkq.selectionsort.SelectionSort2;
import com.wkq.util.ArrayGenerator;
import com.wkq.util.SortingHelper;

/**
 * 插入排序法
 */
public enum InsertionSort implements Sort {
    INSTANCE;

    @Override
    public <E extends Comparable<E>> void sort(E[] arr) {
        //arr[0,i)已排序，[i,n)未排序，每次都将i插入到已排序数组中的合适位置
        for (int i = 1; i < arr.length; i++) {
            E temp = arr[i];//暂存i位置处的元素
            int j;
            for (j = i; j > 0 && temp.compareTo(arr[j - 1]) < 0; j--) {
                arr[j] = arr[j - 1];
            }
            arr[j] = temp;
        }
    }

    public static void main(String[] args) {
        int[] dataSize = {10000, 100000};
        for (int n : dataSize) {
            Integer[] arr = ArrayGenerator.generatorRandomArray(n, n);
            SortingHelper.sortTest(InsertionSort.INSTANCE, arr);
        }
    }
}
