package com.wkq.selectionsort;

import com.wkq.Sort;
import com.wkq.util.ArrayGenerator;
import com.wkq.util.SortingHelper;

/**
 * 换个思路实现
 */
public enum SelectionSort2 implements Sort {
    INSTANCE;

    @Override
    public <E extends Comparable<E>> void sort(E[] arr) {
        //循环不变量，选择arr[0...i]中的最大值,arr(i,n)已排序，arr[0，i]未排序
        for (int i = arr.length - 1; i >= 0; i--) {
            int maxIndex = i;
            //选择arr[0,i]中的最大值
            for (int j = i - 1; j >= 0; j--) {
                maxIndex = arr[maxIndex].compareTo(arr[j]) < 0 ? j : maxIndex;
            }
            SortingHelper.swap(arr, i, maxIndex);
        }
    }

    public static void main(String[] args) {
        int[] dataSize = {10000, 100000};
        for (int n : dataSize) {
            Integer[] arr = ArrayGenerator.generatorRandomArray(n, n);
            SortingHelper.sortTest(SelectionSort2.INSTANCE, arr);
        }
    }

}
