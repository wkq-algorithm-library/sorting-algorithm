package com.wkq.shellsort;

import com.wkq.Sort;
import com.wkq.util.ArrayGenerator;
import com.wkq.util.SortingHelper;

/**
 * h轮
 * 插入排序：（n/h）^2
 * 总共 n^2/h
 *
 */
public enum ShellSortV1 implements Sort {
    INSTANCE;

    @Override
    public <E extends Comparable<E>> void sort(E[] arr) {
        int h = arr.length / 2;
        while (h >= 1) {
            //遍历每个分组的起始元素
            for (int start = 0; start < h; start++) {
                //对每个分组进行插入排序,以start为起始，间隔为h的数组进行插入排序
                for (int i = start + h; i < arr.length; i++) {
                    E temp = arr[i];
                    int j;
                    for (j = i; j - h >= 0 && temp.compareTo(arr[j - 1]) < 0; j -= h) {
                        arr[j] = arr[j - h];
                    }
                    arr[j] = temp;
                }
            }
            h /= 2;
        }
    }

    public static void main(String[] args) {
        int[] dataSize = {10000, 100000};
        for (int n : dataSize) {
            Integer[] arr = ArrayGenerator.generatorRandomArray(n, n);
            SortingHelper.sortTest(ShellSortV1.INSTANCE, arr);
        }
    }
}
