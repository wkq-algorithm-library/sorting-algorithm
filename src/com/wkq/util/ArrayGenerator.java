package com.wkq.util;

import java.util.Random;

public class ArrayGenerator {
    private ArrayGenerator() {
    }

    /**
     * 生成排序数组
     *
     * @param n
     * @return
     */
    public static Integer[] generatorOrderedArray(int n) {
        Integer[] arr = new Integer[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }
        return arr;
    }

    /**
     * 生成一个长度为n的随机数组，每个数字的范围是[0,bound)
     *
     * @param n
     * @param bound
     * @return
     */
    public static Integer[] generatorRandomArray(int n, int bound) {
        Random random = new Random();
        Integer[] arr = new Integer[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(bound);
        }
        return arr;
    }
}
