package com.wkq.util;

import com.wkq.Sort;

public class SortingHelper {
    private SortingHelper() {
    }

    /**
     * 测试排序的方法
     *
     * @param sortInstance 排序工具类
     * @param arr          排序数组
     * @param <E>
     */
    public static <E extends Comparable<E>> void sortTest(Sort sortInstance, E[] arr) {
        long startTime = System.nanoTime();
        sortInstance.sort(arr);
        long endTime = System.nanoTime();
        double time = (endTime - startTime) / 1000000000.0;
        if (!SortingHelper.isSorted(arr)) throw new RuntimeException(sortInstance.getClass() + " failed");
        System.out.println(String.format("%s , n = %d : %f s", sortInstance.getClass().getSimpleName(), arr.length, time));
    }

    /**
     * 判断一个任意类型的数组是否有序
     *
     * @param arr
     * @param <E>
     * @return
     */
    private static <E extends Comparable<E>> boolean isSorted(E[] arr) {
        for (int i = 1; i < arr.length; i++)
            if (arr[i - 1].compareTo(arr[i]) > 0) return false;
        return true;
    }

    /**
     * 交换数组指定索引位置处的元素
     *
     * @param arr
     * @param i
     * @param j
     * @param <E>
     */
    public static <E> void swap(E[] arr, int i, int j) {
        E t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    /**
     * 打印数组
     *
     * @param arr
     * @param <E>
     */
    public static <E> void printArr(E[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i < arr.length - 1) System.out.print(",");
        }
        System.out.println();
    }
}
